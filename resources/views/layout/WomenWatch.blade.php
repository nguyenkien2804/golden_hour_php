<div class="container">
    <div id="woman">
        <div class="title text-center">
            <h1>Đồng hồ nữ</h1>
            <div class="title_border"></div>
        </div>
    </div>
    <div class="row justify-content-evenly">
        <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
            <img src="./img/dong-ho-tissot-t41.1.183.34-nu-tu-dong-day-inox-600x600.jpg" class="card-img-top"
                 alt="...">
            <div class="card-body">
                <p class="card-text text-center">Đồng hồ nữ</p>
                <h6 class="card-title text-center">ĐỒNG HỒ TISSOT T41.1.183.34 NỮ TỰ ĐỘNG DÂY INOX</h6>
                <div class="product_item_details">
                    <p class="price_item">17,640,000 VNĐ</p>
                </div>
                <a href="#" class="btn btn-primary">Mua ngay</a>
            </div>
        </div>

        <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
            <img src="./img/EX1410-88A-600x600.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <p class="card-text text-center">Đồng hồ nữ</p>
                <h6 class="card-title text-center">ĐỒNG HỒ CITIZEN EX1410-88A NỮ ECO-DRIVE DÂY INOX</h6>
                <div class="product_item_details">
                    <p class="price_item">6,120,000 VNĐ</p>
                </div>
                <a href="#" class="btn btn-primary">Mua ngay</a>
            </div>
        </div>

        <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
            <img src="./img/dong-ho-tissot-t063.907.11.058.00-nam-tu-dong-day-inox-600x600-300x300.jpg"
                 class="card-img-top" alt="...">
            <div class="card-body">
                <p class="card-text text-center">Đồng hồ nữ</p>
                <h6 class="card-title text-center">ĐỒNG HỒ CANDINO C4433/3 NỮ PIN DÂY INOX </h6>
                <div class="product_item_details">
                    <p class="price_item">21,940,000 VNĐ</p>
                </div>
                <a href="#" class="btn btn-primary">Mua ngay</a>
            </div>
        </div>

        <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
            <img src="./img/dong-ho-ogival-og385-032lw-t-nu-pin-day-inox-600x600.jpg" class="card-img-top"
                 alt="...">
            <div class="card-body">
                <p class="card-text text-center">Đồng hồ nữ</p>
                <h6 class="card-title text-center">ĐỒNG HỒ OGIVAL OG385-032LW-T NỮ PIN DÂY INOX</h6>
                <div class="product_item_details">
                    <p class="price_item">9,384,000 VNĐ</p>
                </div>
                <a href="#" class="btn btn-primary">Mua ngay</a>
            </div>
        </div>
    </div>
</div>

<div class="container-fuild gap"></div>

<div class="container">
    <div class="row justify-content-evenly">
        <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
            <img src="./img/885sslb-600x600.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <p class="card-text text-center">Đồng hồ nữ</p>
                <h6 class="card-title text-center">ĐỒNG HỒ SKAGEN 885SSLB NỮ PIN DÂY DA</h6>
                <div class="product_item_details">
                    <p class="price_item">5,900,000 VNĐ</p>
                </div>
                <a href="#" class="btn btn-primary">Mua ngay</a>
            </div>
        </div>

        <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
            <img src="./img/dong-ho-daniel-wellington-dw00500001-nu-pin-day-inox-600x600.jpg" class="card-img-top"
                 alt="...">
            <div class="card-body">
                <p class="card-text text-center">Đồng hồ nữ</p>
                <h6 class="card-title text-center">ĐỒNG HỒ DANIEL WELLINGTON DW00500001 NỮ PIN DÂY INOX</h6>
                <div class="product_item_details">
                    <p class="price_item">4,230,000 VNĐ</p>
                </div>
                <a href="#" class="btn btn-primary">Mua ngay</a>
            </div>
        </div>

        <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
            <img src="./img/dong-ho-casio-ga-100de-2adr-nu-pin-day-nhua-600x600.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <p class="card-text text-center">Đồng hồ nữ</p>
                <h6 class="card-title text-center">ĐỒNG HỒ CASIO GA-100DE-2ADR NỮ PIN DÂY NHỰA</h6>
                <div class="product_item_details">
                    <p class="price_item">4,393,000 VNĐ</p>
                </div>
                <a href="#" class="btn btn-primary">Mua ngay</a>
            </div>
        </div>

        <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
            <img src="./img/dong-ho-casio-la670wl-1bdf-nu-pin-day-da-600x600.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <p class="card-text text-center">Đồng hồ nữ</p>
                <h6 class="card-title text-center">ĐỒNG HỒ CASIO LA670WL-1BDF NỮ PIN DÂY DA</h6>
                <div class="product_item_details">
                    <p class="price_item">766,000</p>
                </div>
                <a href="#" class="btn btn-primary">Mua ngay</a>
            </div>
        </div>
    </div>
</div>
