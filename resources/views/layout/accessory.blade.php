<div id="accessory">
    <div class="title text-center">
        <h1>Phụ Kiện</h1>
        <div class="title_border"></div>
    </div>
</div>

<div class="row justify-content-evenly">
    <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
        <img src="./img/Demo-In-Logo-Nestle-600x600-300x300.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text text-center">Phụ kiện</p>
            <h6 class="card-title text-center">Dịch Vụ In Logo Màu Lên Mặt Số Đồng Hồ</h6>
            <div class="product_item_details">
                <p class="price_item">4,320,000 VNĐ</p>
            </div>
            <a href="#" class="btn btn-primary">Mua ngay</a>
        </div>
    </div>

    <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
        <img src="./img/ZRC-DOUBLE-WATCH-WINDER-EM03201-600x600-300x300.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text text-center">Phụ kiện</p>
            <h6 class="card-title text-center">HỘP LÊN DÂY CÓT TỰ ĐỘNG ZRC DOUBLE-WATCH WINDER EM03201</h6>
            <div class="product_item_details">
                <p class="price_item">6,140,000 VNĐ</p>
            </div>
            <a href="#" class="btn btn-primary">Mua ngay</a>
        </div>
    </div>

    <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
        <img src="./img/day-da-zrc-654-tasman-600x600-300x300.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text text-center">Phụ kiện</p>
            <h6 class="card-title text-center">DÂY DA ZRC 654 TASMAN OLYMPIA STAR OPA58012-07MSK</h6>
            <div class="product_item_details">
                <p class="price_item">5,140,000 VNĐ</p>
            </div>
            <a href="#" class="btn btn-primary">Mua ngay</a>
        </div>
    </div>

    <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
        <img src="./img/18_Hirsh-Mariner-Black-600x600-300x300.jpg" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text text-center">Phụ kiện</p>
            <h6 class="card-title text-center">HIRSCH MARINER OLYMPIA STAR OPA58012-07MSK</h6>
            <div class="product_item_details">
                <p class="price_item">950,000</p>
            </div>
            <a href="#" class="btn btn-primary">Mua ngay</a>
        </div>
    </div>
</div>
