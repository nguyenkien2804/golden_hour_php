<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Golden Hour</title>

    <link rel="shortcut icon" href="{{asset('img/logo.png')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('asset/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('asset/font/fontawesome-free-5.15.4/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('/asset/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/gold.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/profile.css')}}">
</head>
