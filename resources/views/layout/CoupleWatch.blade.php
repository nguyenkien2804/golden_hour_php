<div id="couple">
    <div class="title text-center">
        <h1>Đồng hồ đôi</h1>
        <div class="title_border"></div>
    </div>
</div>

<div class="row justify-content-evenly">

    <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
        <img src="./img/dong-ho-doi-sunrise-sg-sl1085.1601-pin-day-inox-600x600-300x300 (1).jpg"
             class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text text-center">Đồng hồ đôi</p>
            <h6 class="card-title text-center">ĐỒNG HỒ ĐÔI SUNRISE SG – SL1085.1601 PIN DÂY INOX</h6>
            <div class="product_item_details">
                <p class="price_item">4,3320,000 VNĐ</p>
            </div>
            <a href="#" class="btn btn-primary">Mua ngay</a>
        </div>
    </div>

    <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
        <img src="./img/dong-ho-citizen-bd0030-00a-er0190-00a-doi-pin-day-da-a-600x600-300x300.jpg"
             class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text text-center">Đồng hồ đôi</p>
            <h6 class="card-title text-center">ĐỒNG HỒ ĐÔI CITIZEN BD0030-00A – ER0190-00A PIN DÂY DA</h6>
            <div class="product_item_details">
                <p class="price_item">5,140,000 VNĐ</p>
            </div>
            <a href="#" class="btn btn-primary">Mua ngay</a>
        </div>
    </div>

    <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
        <img src="./img/dong-ho-casio-mtp-e312d-7bvdf-ltp-e312d-4bvdf-doi-pin-day-inox-a-600x600-300x300.jpg"
             class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text text-center">Đồng hồ đôi</p>
            <h6 class="card-title text-center">ĐỒNG HỒ ĐÔI CASIO MTP-E312D-7BVDF – LTP-E312D-4BVDF PIN DÂY INOX
            </h6>
            <div class="product_item_details">
                <p class="price_item">4,324,000</p>
            </div>
            <a href="#" class="btn btn-primary">Mua ngay</a>
        </div>
    </div>

    <div class="card col-lg-3 col-xs-2" style="width: 18rem;">
        <img src="./img/dong-ho-doi-olympia-star-opa58012-07msk-trang-opa58012-07lsk-trang-pin-day-inox-600x600-300x300 (1).jpg"
             class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text text-center">Đồng hồ đôi</p>
            <h6 class="card-title text-center">ĐỒNG HỒ ĐÔI OLYMPIA STAR OPA58012-07MSK TRẮNG – OPA58012-07LSK
                TRẮNG
                PIN DÂY
                INOX</h6>
            <div class="product_item_details">
                <p class="price_item">58,766,000</p>
            </div>
            <a href="#" class="btn btn-primary">Mua ngay</a>
        </div>
    </div>
</div>
