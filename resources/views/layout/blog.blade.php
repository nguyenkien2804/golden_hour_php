<div class="row justify-content-evenly">
    <div class="col-lg-4">
        <div class="card" style="width: 26rem;">
            <img src="./img/slide1.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <p class="price_item">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut molestiae consequatur
                    dignissimos iusto minus libero culpa, enim beatae, nulla laudantium repellat doloribus sint quae. Atque
                    provident ad ut deleniti animi?</p>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card" style="width: 26rem;">
            <img src="./img/slide2.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <p class="price_item">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut molestiae consequatur
                    dignissimos iusto minus libero culpa, enim beatae, nulla laudantium repellat doloribus sint quae. Atque
                    provident ad ut deleniti animi?</p>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card" style="width: 26rem;">
            <img src="./img/slide3.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <p class="price_item">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut molestiae consequatur
                    dignissimos iusto minus libero culpa, enim beatae, nulla laudantium repellat doloribus sint quae. Atque
                    provident ad ut deleniti animi?</p>
            </div>
        </div>
    </div>
</div>
