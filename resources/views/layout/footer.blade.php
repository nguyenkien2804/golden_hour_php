<footer style="background-color: #1f2024;">
    <div class="container">
        <div class="row footer_nav_menu">
            <div class="col-3">
                <h3 class="widget-title">
                    My Account
                </h3>
                <ul>
                    <li class="menu-item"><a href="">Product Support</a></li>
                    <li class="menu-item"><a href="">Checkout</a></li>
                    <li class="menu-item"><a href="">Shoping cart</a></li>
                    <li class="menu-item"><a href="">Wistlist</a></li>
                    <li class="menu-item"><a href="">Term & Conditions</a></li>
                    <li class="menu-item"><a href="">Redeem Voucher</a></li>
                </ul>
            </div>
            <div class="col-3">
                <h3 class="widget-title">
                    Customer Care
                </h3>
                <ul>
                    <li class="menu-item"><a href="">New Customers</a></li>
                    <li class="menu-item"><a href="">How to use Account</a></li>
                    <li class="menu-item"><a href="">Placing a Order</a></li>
                    <li class="menu-item"><a href="">Payment Method</a></li>
                    <li class="menu-item"><a href="">Delivery & dispatch</a></li>
                    <li class="menu-item"><a href="">Problem with Order</a></li>
                </ul>
            </div>
            <div class="col-3">
                <h3 class="widget-title">
                    Customer Service
                </h3>
                <ul>
                    <li class="menu-item"><a href="">Help Center</a></li>
                    <li class="menu-item"><a href="">Contact us</a></li>
                    <li class="menu-item"><a href="">Report Abuse</a></li>
                    <li class="menu-item"><a href="">Submit a Dispute</a></li>
                    <li class="menu-item"><a href="">Policies & Rules</a></li>
                </ul>
            </div>
            <div class="col-3">
                <h3 class="widget-title">
                    Sign Up To Newsletter
                </h3>
                <p class="footer_text">Join 60.000+ subscribers and get a new discount coupon on every Saturday.</p>
            </div>
        </div>
    </div>
</footer>
